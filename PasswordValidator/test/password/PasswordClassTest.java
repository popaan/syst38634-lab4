package password;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PasswordClassTest {

	@Test
	public void testIsValidLengthRegular() {
		String password = "1234567890";
		boolean check = PasswordClass.isValidLength(password);
		
		assertTrue("Invalid password length", check);
	}
	
	@Test
	public void testIsValidLengthException() {
		String password = "1234";
		boolean check = PasswordClass.isValidLength(password);
		
		assertFalse("Invalid password length", check);
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		String password = "        ";
		boolean check = PasswordClass.isValidLength(password);
		
		assertFalse("Invalid password length", check);
	}
	
	@Test
	public void testIsValidLengthExceptionNull() {
		String password = null;
		boolean check = PasswordClass.isValidLength(password);
		
		assertFalse("Invalid password length", check);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		String password = "1234567";
		boolean check = PasswordClass.isValidLength(password);
		
		assertFalse("Invalid password length", check);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		String password = "12345678";
		boolean check = PasswordClass.isValidLength(password);
		
		assertTrue("Invalid password length", check);
	}
	
	/*
	 * @author Andreea Popa 991519111
	 */
	 @Test
	public void testHasValidDigitCountRegular() {
		String password = "password123";
		boolean check = PasswordClass.hasValidDigitCount(password);
		
		assertTrue("Invalid amount of digits", check);
	} 
	
	@Test
	public void testHasValidDigitCountException() {
		String password = "password";
		boolean check = PasswordClass.hasValidDigitCount(password);
		
		assertFalse("Invalid amount of digits", check);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		String password = "password1";
		boolean check = PasswordClass.hasValidDigitCount(password);
		
		assertFalse("Invalid amount of digits", check);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		String password = "password12";
		boolean check = PasswordClass.hasValidDigitCount(password);
		
		assertTrue("Invalid amount of digits", check);
	}
	
	@Test
	public void testHasValidCaseCharactersRegular() {
		assertTrue("Invalid case characters", PasswordClass.hasValidCaseCharacters("pasSword"));
	}
	
	@Test
	public void testHasValidCaseCharactersBoundaryIn() {
		assertTrue("Invalid case characters", PasswordClass.hasValidCaseCharacters("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharactersException() {
		assertFalse("Invalid case characters", PasswordClass.hasValidCaseCharacters("12345"));
	}
	
	@Test
	public void testHasValidCaseCharactersExceptionEmpty() {
		assertFalse("Invalid case characters", PasswordClass.hasValidCaseCharacters(""));
	}
	
	@Test
	public void testHasValidCaseCharactersExceptionNull() {
		assertFalse("Invalid case characters", PasswordClass.hasValidCaseCharacters(null));
	}
	
	@Test
	public void testHasValidCaseCharactersBoundaryOutUpper() {
		assertFalse("Invalid case characters", PasswordClass.hasValidCaseCharacters("A"));
	}
	
	@Test
	public void testHasValidCaseCharactersBoundaryOutLower() {
		assertFalse("Invalid case characters", PasswordClass.hasValidCaseCharacters("a"));
	}

}
