package password;

/*
 * @author Andreea Popa 991519111
 * 
 * Assumptions: 
 * - spaces should not be considered valid characters
 * - password should not be empty
 * 
 * This is a change
 */

public class PasswordClass {
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	
	/*
	 * @author Andreea Popa 991519111
	 */
	public static boolean isValidLength(String password) {
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		return (password.replaceAll("\\D", "").length() >= MIN_DIGITS);
	}
	
	public static boolean hasValidCaseCharacters(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
}
